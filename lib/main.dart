import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter OS_21 Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int counter = 0;
  List names = ['Jan', 'Chris', 'Stefan', 'Harry', 'Karl'];
  String currentName = '';

  void incrementCounter() {
    setState(() {
      counter++;
      if(counter > 3){
        counter = 0;
      }
      currentName = names[counter];

    });
  }

  void resetCounter() {
    setState(() {
      counter = 0;
      currentName = names[counter];
    });
  }


  void pushView() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SecondRoute()),
    );
  }

  @override
  Widget build(BuildContext context) {
    //LayoutWidget
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Name Generated: ',
            ),
            Text(
              '$currentName',
              style: Theme.of(context).textTheme.display1,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child: Row(
          children: <Widget>[
            Text("Reset:"),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: resetCounter,
            ),
           RaisedButton(
            child: Text('Open route'),
            onPressed: pushView,
          ),
        ],
        ),
      )
    );
  }
}







class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Controller"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child:  Row(
              children: <Widget>[
                Text('Go back!'),
              ]
        )
        ),
      ),
    );
  }
}
